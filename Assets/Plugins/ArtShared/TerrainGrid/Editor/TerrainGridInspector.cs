﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using ArtSharedDll;

namespace ArtSharedDll
{
	[CustomEditor (typeof(TerrainGrid))]
	public class TerrainGridInspector : Editor
	{

		//-----------每个格子的大小------------
		private const float GRID_SIZE = 1f;

		//层
		private const string GAMEOBJECT_NAME = "Grid";

		private Vector3 lastDrogPostion = Vector3.zero;

		private TerrainGrid terrainGrid = null;

		void OnEnable ()
		{
			terrainGrid = target as TerrainGrid;
		}


		void OnSceneGUI ()
		{

			if (Event.current != null) {
				Event e = Event.current;

				if (Tools.viewTool != ViewTool.Orbit && e.button == 0) {
					if (e.type == EventType.mouseMove || e.type == EventType.mouseDown || e.type == EventType.mouseUp || e.type == EventType.mouseDrag) {	
						Ray worldRay = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
						RaycastHit hitInfo;
						if (Physics.Raycast (worldRay, out hitInfo, 1000, 1 << LayerMask.NameToLayer (GAMEOBJECT_NAME))) {             
							Vector3	postion = hitInfo.point - hitInfo.collider.transform.position;

							if (e.type == EventType.mouseDown) {
								terrainGrid.CacheDown(postion);
								terrainGrid.Click (postion);
							} else
							{
								if(Vector3.Distance(lastDrogPostion,postion) >= GRID_SIZE/2f)
								{
									if (e.type == EventType.mouseDrag) {
										terrainGrid.Drag (postion);
									} else if (e.type == EventType.mouseMove) {
										terrainGrid.Brush(postion);
									}
									lastDrogPostion = postion;
								}
							}
						}else
						{
							terrainGrid.ClearBrush();
						}
					}
				}


				int controlID = GUIUtility.GetControlID (FocusType.Passive);
				if (e.type == EventType.Layout) {
					HandleUtility.AddDefaultControl (controlID);
				}
			}


		}

		[MenuItem ("Window/NavGrid/LoadGrid", true)]
		static bool  ValidateLoadGrid () 
		{
			//可以在这里判断一下场景，因为不是所有场景都需要读取地形Grid的
			//return (EditorApplication.currentScene.IndexOf("raw_") !=-1);
			return true;
		}


		[MenuItem("Window/NavGrid/LoadGrid")]
		static void LoadGrid()
		{

			Terrain terrain = null;
			Terrain []all = GameObject.FindObjectsOfType<Terrain>();

			for(int i = 0 ;i < all.Length; i++)
			{
				if(all[i].gameObject.layer != LayerMask.NameToLayer(GAMEOBJECT_NAME)){
					terrain = all[i];
					break;
				}

			}

			if(terrain)
			{
				if(terrain.terrainData.size.x != terrain.terrainData.size.z)
				{
					Debug.LogError("地形只支持正方形");
					return;
				}

				if(terrain.terrainData.size.x < 16)
				{
					Debug.LogError("地形需要大于16");
					return;
				}


	//----------写文件的代码，备份一下
//				string tempTerrainPath = "Assets/TempTerrain.asset";
//	
//	
//				AssetDatabase.DeleteAsset(tempTerrainPath);
//	
//				AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(terrain.terrainData),tempTerrainPath);
//	
//	
//				AssetDatabase.SaveAssets();
//				AssetDatabase.Refresh();
//	
//				TerrainData terrainData = AssetDatabase.LoadAssetAtPath<TerrainData>(tempTerrainPath);
	//----------写文件的代码，备份一下
			


				TerrainData terrainData = new TerrainData();

			
				terrainData.heightmapResolution =terrain.terrainData.heightmapResolution;

				terrainData.size = terrain.terrainData.size;


				float [,] heights = terrain.terrainData.GetHeights(0, 0, terrain.terrainData.heightmapWidth,  
				terrain.terrainData.heightmapHeight);  
				terrainData.SetHeights(0, 0, heights);  

				int gridCount = (int)(terrain.terrainData.size.x / GRID_SIZE);


				string gridPath = "Assets/Plugins/ArtShared/TerrainGrid";
				terrainData.splatPrototypes = new SplatPrototype[]
				{
					new SplatPrototype(){texture = AssetDatabase.LoadAssetAtPath<Texture2D>(gridPath + "/red.png"),tileSize = Vector2.one * GRID_SIZE},
					new SplatPrototype(){texture = AssetDatabase.LoadAssetAtPath<Texture2D>(gridPath + "/white.png"),tileSize =  Vector2.one * GRID_SIZE},
					new SplatPrototype(){texture = AssetDatabase.LoadAssetAtPath<Texture2D>(gridPath + "/brush.png"),tileSize =  Vector2.one * GRID_SIZE}


				};

				terrainData.alphamapResolution  = gridCount;

				terrainData.alphamapTextures [0].filterMode = FilterMode.Point;

				GameObject gridObject =  FindRootGameObject(GAMEOBJECT_NAME);
				if(!gridObject)
				{
					gridObject = new GameObject(GAMEOBJECT_NAME);
				}else
				{
					gridObject.SetActive(true);
				}

				gridObject.layer = LayerMask.NameToLayer(GAMEOBJECT_NAME);
				gridObject.transform.position = Vector3.zero;

				Transform terrainTransform = gridObject.transform.FindChild("Terrain");
				if(!terrainTransform)
				{

				   
					GameObject go =  Terrain.CreateTerrainGameObject(terrainData);
				   go.transform.SetParent(gridObject.transform);
	
				   terrainTransform = go.transform;
				}

				terrainTransform.transform.position = new Vector3(terrain.transform.position.x ,
				terrain.transform.position.y + 0.1f,terrain.transform.position.z);
				terrainTransform.gameObject.hideFlags = HideFlags.DontSave |  HideFlags.NotEditable;

				terrainTransform.gameObject.layer = LayerMask.NameToLayer(GAMEOBJECT_NAME);
				TerrainGrid grid = 	gridObject.GetComponent<TerrainGrid>() ?? gridObject.AddComponent<TerrainGrid>();

				grid.gridSize=GRID_SIZE;

				grid.gridCount = gridCount;

				grid.Init();
			}
		}


		 static GameObject FindRootGameObject(string name)
	     {
	         GameObject[] pAllObjects = (GameObject[])Resources.FindObjectsOfTypeAll(typeof(GameObject));
	 
	         foreach (GameObject pObject in pAllObjects)
	         {
	             
	             if (pObject.transform.parent != null)
	             {
	                 continue;
	             }
	 
	             if (pObject.hideFlags == HideFlags.NotEditable || pObject.hideFlags == HideFlags.HideAndDontSave)
	             {
	                 continue;
	             }
	 
	             if (Application.isEditor)
	             {
	                 string sAssetPath = AssetDatabase.GetAssetPath(pObject.transform.root.gameObject);
	                 if (!string.IsNullOrEmpty(sAssetPath))
	                 {
	                     continue;
	                 }
	             }
	             
				if(pObject.name == name){
					return pObject;
				}
	         }
	 		return null;
	     }


	}
}